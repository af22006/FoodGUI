import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton ebitiriButton;
    private JButton ramenButton;
    private JButton subutaButton;
    private JTextArea textbox1;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JButton rebaniraButton;
    private JButton checkOutButton;
    private JTextArea textbox2;
    int sum = 0;

     void order(String food, int price) {
         int Confirmation = JOptionPane.showConfirmDialog(
                 null,
                 "Would you like to order " + food + "?",
                 "Order Confirmation",
                 JOptionPane.YES_NO_OPTION);
         int rice=JOptionPane.showConfirmDialog(
                 null,
                 "Would you like to add rice for 100yen?",
                 "Order Confirmation",
                 JOptionPane.YES_NO_OPTION);
         if (Confirmation == 0) {
             String currentText = textbox1.getText();
             textbox1.setText(currentText + " "+food + " " + price + "yen"+"\n");

             sum += price;
             textbox2.setText("total: " + sum + "yen");
         }
         if(rice==0){
             String currentText = textbox1.getText();
             textbox1.setText(currentText + "rice 100yen\n");

             sum = sum+ 100;
             textbox2.setText("total: " + sum + "yen");

         }
     }


    public FoodGUI() {
        ebitiriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ebitiri", 800);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 800);
            }
        });
        subutaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Subuta", 700);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 650);
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba", 600);
            }
        });
        rebaniraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("rebaniraitame",700);

            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int Confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Check out confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (Confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + sum + " yen");
                    textbox1.setText("");
                    textbox2.setText("total: 0yen");
                    sum = 0;
                }
            }
        });
        textbox2.setText("total: 0yen");
    }
    public static void main (String[]args){
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

